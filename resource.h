//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Test.rc
//
#define IDD_DIALOG_MAIN                 101
#define IDD_DIALOG_PEEDIT               102
#define IDC_BUTTON_PE                   1000
#define IDC_BUTTON_ABOUT                1001
#define IDC_BUTTON_LOGOUT               1002
#define IDC_LIST_PROCESS                1003
#define IDC_LIST_MOUDLE                 1004
#define IDC_BUTTON_ADDSHELL             1005
#define IDC_BUTTON_DLLINJECT            1006
#define IDC_EDIT_ENTRANCE               1009
#define IDC_EDIT_IMAGEBASE              1010
#define IDC_EDIT_SIZEOFIMAGE            1011
#define IDC_EDIT_BASEOFCODE             1012
#define IDC_EDIT_BASEOFDATA             1013
#define IDC_EDIT_SECTIONALIGNMENT       1014
#define IDC_EDIT_FILEALIGNMENT          1015
#define IDC_EDIT_MAGIC                  1016
#define IDC_EDIT_SUBSYSTEM              1017
#define IDC_EDIT_NUMBEROFSECTION        1018
#define IDC_EDIT_TIMEDATESTAMP          1019
#define IDC_EDIT_SIZEOFHEADER           1020
#define IDC_EDIT_FILLCHARACTERISTICS    1021
#define IDC_EDIT_CHECKSUM               1022
#define IDC_EDIT_SIZEOFOPTIONHEADER     1023
#define IDC_EDIT_NUMBEROFRVAANDSIZE     1024
#define IDC_BUTTON_OK                   1025
#define IDC_BUTTON_SAVE                 1026
#define IDC_BUTTON3                     1027
#define IDC_BUTTON4                     1028
#define IDC_BUTTON5                     1029
#define IDC_BUTTON6                     1030
#define IDC_BUTTON7                     1031
#define IDC_BUTTON8                     1032

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
