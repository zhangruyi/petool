#pragma once
#define _CRT_SECURE_NO_WARNINGS//高版的VS默认不让使用scanf,fopen等函数,说是scanf,fopen等函数不安全,而代替其函数的是scanf_s,fopen_s等函数,后边有个"_s"的形式。想要使用, 可以在源文件开头加个:

#include <Windows.h>


//函数声明

//**************************************************************************

//ReadPEFile:将文件读取到缓冲区

//参数说明：

//lpszFile 文件路径 //pFileBuffer 缓冲区指针

//返回值说明：

//读取失败返回0  否则返回实际读取的大小

//******************************************************
DWORD ReadPEFile(IN LPSTR lpszFile, OUT LPVOID* pFileBuffer);



//******************************************************

//CopyFileBufferToImageBuffer:将文件从FileBuffer复制到ImageBuffer

//参数说明：

//pFileBuffer  FileBuffer指针

//pImageBuffer ImageBuffer指针

//返回值说明： //读取失败返回0  否则返回复制的大小 
//****************************************************** 
DWORD CopyFileBufferToImageBuffer(IN LPVOID pFileBuffer, OUT LPVOID* pImageBuffer);



//******************************************************

//CopyImageBufferToNewBuffer:将ImageBuffer中的数据复制到新的缓冲区

//参数说明：

//pImageBuffer ImageBuffer指针

//pNewBuffer NewBuffer指针

//返回值说明：

//读取失败返回0  否则返回复制的大小

//******************************************************
DWORD CopyImageBufferToNewBuffer(IN LPVOID pImageBuffer, OUT LPVOID* pNewBuffer);


//******************************************************

//MemeryTOFile:将内存中的数据复制到文件

//参数说明：

//pMemBuffer 内存中数据的指针

//size 要复制的大小

//lpszFile 要存储的文件路径

//返回值说明：

//读取失败返回0  否则返回复制的大小

//******************************************************
BOOL MemeryTOFile(IN LPVOID pMemBuffer, IN size_t size, OUT LPSTR lpszFile);




//******************************************************

//RvaToFileOffset:将内存偏移转换为文件偏移

//参数说明：

//pFileBuffer FileBuffer指针

//dwRva RVA的值

//返回值说明：

//返回转换后的FOA的值  如果失败返回0

//****************************************************** 
DWORD RvaToFileOffset(IN LPVOID pFileBuffer, IN DWORD dwRva);



BOOL ReadPeStruc(HWND hWnd,LPSTR lpszFile);


//******************************************************

//RvaToFileOffset:增加新节

//参数说明：

//pImageBuffer pFileBuffer指针

//pNewBuffer pImageBuffer指针

//返回值说明：

//读取失败返回0  否则返回复制的大小

//****************************************************** 
DWORD AddNewSection(IN LPVOID pFileBuffer, OUT LPVOID* pImageBuffer,size_t size,size_t shellsize);
