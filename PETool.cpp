// PETool.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"
#include <Commdlg.h>
#include<Tlhelp32.h>
#include<stdlib.h>
#include "pestudy.h"

#include<commctrl.h>
#pragma comment(lib,"COMCTL32.lib")

#include <tchar.h>


HINSTANCE m_hInstance;
LPSTR filePath;

VOID EnumProcess(HWND hListProcess)
{

	HANDLE hSnapShot;
	//HANDLE hProcess;
	PROCESSENTRY32 pro32;
	//DWORD dwPriorityClass;
	pro32.dwSize=sizeof(PROCESSENTRY32);

	hSnapShot= CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hSnapShot == INVALID_HANDLE_VALUE) {
        return;
    }

	int index=0;
	BOOL bMore;
	bMore=Process32First(hSnapShot,&pro32);
	while(bMore)
	{
		

		TCHAR szImageBase[0x20];    //进程基址
        memset(szImageBase, 0, 0x20);
        TCHAR szImageSize[0x20];    //进程大小
        memset(szImageSize, 0, 0x20);

		MODULEENTRY32 me32 = { 0 };
        me32.dwSize = sizeof(MODULEENTRY32);
        HANDLE hModuleSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pro32.th32ProcessID);
		if(hModuleSnap==INVALID_HANDLE_VALUE)
		{
			int errcode=GetLastError();
			bMore=Process32Next(hSnapShot,&pro32);
			continue;
		}
        if(hModuleSnap != INVALID_HANDLE_VALUE){
            // 获取模块快照中第一条信息
            if(Module32First(hModuleSnap, &me32)){
                DWORD pProcessImageBase = (DWORD)me32.modBaseAddr;
                sprintf(szImageBase,"0x%08X",pProcessImageBase);
				//MessageBox(NULL,TEXT("dd"),szImageBase,0);
				//_itot(pProcessImageBase, szImageBase, 10);
 
                DWORD pProcessSize = (DWORD)me32.modBaseSize;
                sprintf(szImageSize,"0x%08X",pProcessSize);
            }
        }
        // 关闭句柄
        ::CloseHandle(hModuleSnap);

		//第一列
		LV_ITEM vitem;
		memset(&vitem,0,sizeof(LV_ITEM));
		vitem.mask=LVIF_TEXT;
		vitem.pszText=pro32.szExeFile;
		vitem.iItem=index;
		vitem.iSubItem=0;
		//ListView_InsertItem(hListProcess,&vitem);
		SendMessage(hListProcess,LVM_INSERTITEM,0,(DWORD)&vitem);

		
		//第二列
	    TCHAR szPID[10] = {0};
		sprintf(szPID, "%08x",  pro32.th32ProcessID);
		vitem.pszText= szPID;
		vitem.iItem=index;
		vitem.iSubItem=1;
		ListView_SetItem(hListProcess,&vitem);
		//SendMessage(hListProcess,LVM_INSERTITEM,0,(DWORD)&vitem);


		//第三列

		vitem.pszText=szImageBase;
		//vitem.pszText=buffer;
		vitem.iItem=index;
		vitem.iSubItem=2;
		ListView_SetItem(hListProcess,&vitem);

		//第四列
		vitem.pszText=szImageSize;
		vitem.iItem=index;
		vitem.iSubItem=3;
		ListView_SetItem(hListProcess,&vitem);

		
		bMore=Process32Next(hSnapShot,&pro32);
		index++;
	}

	CloseHandle(hSnapShot);



}

VOID InitProcessListView(HWND hDlg)
{
	LV_COLUMN lv;
	HWND hListProcess;

	//初始化
	memset(&lv,0,sizeof(LV_COLUMN));

	//获取IDC_LIST_PROCESS句柄
	hListProcess=GetDlgItem(hDlg,IDC_LIST_PROCESS);
	
	//设置整行选中,sendmessage 无论想让窗口作什么事情，都用sendmessage。所有窗口都是Windows管理的，Windows是基于消息的
	//
	SendMessage(hListProcess,LVM_SETEXTENDEDLISTVIEWSTYLE,LVS_EX_FULLROWSELECT,LVS_EX_FULLROWSELECT);

	//第一列
	lv.mask=LVCF_TEXT|LVCF_WIDTH|LVCF_SUBITEM;
	lv.pszText=TEXT("进程");
	lv.cx=200;
	lv.iSubItem=0;
	//ListView_InsertColumn(hListProcess,0,&lv);
	SendMessage(hListProcess,LVM_INSERTCOLUMN,0,(DWORD)&lv);

	//第二列
	lv.pszText=TEXT("PID");
	lv.cx=100;
	lv.iSubItem=1;
	//ListView_InsertColumn(hListProcess,1,&lv);
	SendMessage(hListProcess,LVM_INSERTCOLUMN,1,(DWORD)&lv);

	//第三列
	lv.pszText=TEXT("镜像基址");
	lv.cx=100;
	lv.iSubItem=2;
	ListView_InsertColumn(hListProcess,2,&lv);

	//第四列
	lv.pszText=TEXT("镜像大小");
	lv.cx=100;
	lv.iSubItem=3;
	ListView_InsertColumn(hListProcess,3,&lv);

	EnumProcess(hListProcess);
}


VOID InitModuleListView(HWND hDlg)
{
	LV_COLUMN lv;
	HWND hListProcess;

	//初始化
	memset(&lv,0,sizeof(LV_COLUMN));

	//获取IDC_LIST_PROCESS句柄
	hListProcess=GetDlgItem(hDlg,IDC_LIST_MOUDLE);
	
	//设置整行选中,sendmessage 无论想让窗口作什么事情，都用sendmessage。所有窗口都是Windows管理的，Windows是基于消息的
	//
	SendMessage(hListProcess,LVM_SETEXTENDEDLISTVIEWSTYLE,LVS_EX_FULLROWSELECT,LVS_EX_FULLROWSELECT);

	//第一列
	lv.mask=LVCF_TEXT|LVCF_WIDTH|LVCF_SUBITEM;
	lv.pszText=TEXT("路径");
	lv.cx=200;
	lv.iSubItem=0;
	//ListView_InsertColumn(hListProcess,0,&lv);
	SendMessage(hListProcess,LVM_INSERTCOLUMN,0,(DWORD)&lv);

	//第二列
	lv.pszText=TEXT("镜像基址");
	lv.cx=100;
	lv.iSubItem=1;
	//ListView_InsertColumn(hListProcess,1,&lv);
	SendMessage(hListProcess,LVM_INSERTCOLUMN,1,(DWORD)&lv);

	//第三列
	lv.pszText=TEXT("镜像大小");
	lv.cx=100;
	lv.iSubItem=2;
	//ListView_InsertColumn(hListProcess,1,&lv);
	SendMessage(hListProcess,LVM_INSERTCOLUMN,1,(DWORD)&lv);
}

unsigned long GetSelectProcessId(HWND hListProcess)
{
	DWORD dwRowId;
	TCHAR szPid[0x20];
	LV_ITEM lv;

	//初始化
	memset(&lv,0,sizeof(LV_ITEM));
	memset(szPid,0,0x20);

	dwRowId=SendMessage(hListProcess,LVM_GETNEXTITEM,-1,LVNI_SELECTED);
	if(dwRowId==-1)
	{
	MessageBox(NULL,TEXT("请选择进程"),TEXT("出错了"),MB_OK);
	return 0;
	}

	//获取PID
	lv.iSubItem=1;
	lv.pszText=szPid;
	lv.cchTextMax=0x20;
	SendMessage(hListProcess,LVM_GETITEMTEXT,dwRowId,(DWORD)&lv);

	//char *stopstring;

	unsigned long lPid;
	sscanf(szPid,"%X",&lPid);
	return lPid;
}

VOID EnumModules(HWND hListProcess,HWND hListModule,WPARAM wPa, LPARAM lPa)
{
	DWORD dwRowId;
	TCHAR szPid[0x20];
	LV_ITEM lv;

	//初始化
	memset(&lv,0,sizeof(LV_ITEM));
	memset(szPid,0,0x20);

	dwRowId=SendMessage(hListProcess,LVM_GETNEXTITEM,-1,LVNI_SELECTED);
	if(dwRowId==-1)
	{
	MessageBox(NULL,TEXT("请选择进程"),TEXT("出错了"),MB_OK);
	return;
	}

	//获取PID
	lv.iSubItem=1;
	lv.pszText=szPid;
	lv.cchTextMax=0x20;
	SendMessage(hListProcess,LVM_GETITEMTEXT,dwRowId,(DWORD)&lv);

	//char *stopstring;

	unsigned long lPid;
	sscanf(szPid,"%X",&lPid);
	//lPid=strtoul( szPid,&stopstring,10);
	HANDLE hModuleSnap = ::CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, lPid);
	if(hModuleSnap==INVALID_HANDLE_VALUE)
	{
		int errcode=GetLastError();
		return;
	}

	TCHAR szImageBase[0x20];    //进程基址
    
    TCHAR szImageSize[0x20];    //进程大小
    

    if(hModuleSnap != INVALID_HANDLE_VALUE){
		ListView_DeleteAllItems(hListModule);
		MODULEENTRY32 me32 = { 0 };
        me32.dwSize = sizeof(MODULEENTRY32);
        // 获取模块快照中第一条信息
        if(Module32First(hModuleSnap, &me32)){
			int index=0;
			do{

				memset(szImageBase, 0, 0x20);
				memset(szImageSize, 0, 0x20);

				DWORD pProcessImageBase = (DWORD)me32.modBaseAddr;
				sprintf(szImageBase,"0x%08X",pProcessImageBase);

				DWORD pProcessSize = (DWORD)me32.modBaseSize;
				sprintf(szImageSize,"0x%08X",pProcessSize);

				LV_ITEM vitem;

				//第一列
				vitem.mask=LVIF_TEXT;
				vitem.pszText=me32.szExePath;
				vitem.iItem=index;
				vitem.iSubItem=0;
				//ListView_InsertItem(hListProcess,&vitem);
				SendMessage(hListModule,LVM_INSERTITEM,0,(DWORD)&vitem);


				//第二列
				vitem.mask=LVIF_TEXT;
				vitem.pszText=szImageBase;
				vitem.iItem=index;
				vitem.iSubItem=1;
				ListView_SetItem(hListModule,&vitem);
				//SendMessage(hListModule,LVM_INSERTITEM,0,(DWORD)&vitem);


				//第三列
				vitem.mask=LVIF_TEXT;
				vitem.pszText=szImageSize;
				vitem.iItem=index;
				vitem.iSubItem=2;
				ListView_SetItem(hListModule,&vitem);
				//SendMessage(hListModule,LVM_INSERTITEM,0,(DWORD)&vitem);
				
				index++;
			}while(Module32Next(hModuleSnap, &me32));
            
        }
    }
    // 关闭句柄
    ::CloseHandle(hModuleSnap);
	//MessageBox(NULL,szPid,TEXT("PID"),MB_OK);

}



BOOL CALLBACK PeEditDlgProc(HWND hWnd, UINT Message, WPARAM wPa, LPARAM lPa)
{
	BOOL bRet=FALSE;
	switch(Message)
	{
	case WM_CLOSE:
		EndDialog(hWnd,0);
		break;
	case WM_INITDIALOG:
		//dialog初始化
		ReadPeStruc(hWnd,filePath);
		break;
	case WM_NOTIFY:
	{
		
		break;
	}
	case WM_COMMAND:
		//按钮处理消息
		switch(LOWORD(wPa))
		{
		case IDC_BUTTON_SAVE:
			//保存
			break;
		case IDC_BUTTON_OK:
			//确定
			EndDialog(hWnd,0);
			break;
		}
		break;
	}
	return bRet;
}

void xor_crypt(const char *key, int key_len, char *data, int data_len)
{
    for (int i = 0; i < data_len; i++)
        data[i] ^= key[ i % key_len ];
}

bool LoadDll(DWORD ProcessId,LPSTR dllPath)
{
    HANDLE hProcess;//进程句柄
    DWORD DllLength;//dll路径字符串长度
    PDWORD DllAddr;//dll地址字符串分配的虚拟地址

    HMODULE hModule;//kernel32.dll 句柄
    PDWORD FuncAddr;//LoadLibrary() 函数句柄



    hProcess = OpenProcess(PROCESS_ALL_ACCESS, false, ProcessId);

    if (hProcess == NULL)
    {
        return false;
    }

    DllLength = (wcslen((unsigned short *)dllPath)+1)*2;

    //申请指定进程中的内存
    DllAddr = (PDWORD)VirtualAllocEx(hProcess, NULL, DllLength, MEM_COMMIT, PAGE_READWRITE);

    if (DllAddr == NULL)
        return false;

    printf("%x", DllAddr);

    //写入dll路径
    WriteProcessMemory(hProcess, DllAddr, dllPath, DllLength, NULL);

    //获取Kernel32.dll的句柄
    hModule = GetModuleHandle("Kernel32.dll");
    if (hModule == NULL)
        return false;
    //从module中获取loadlabrary函数地址
    FuncAddr =(PDWORD) GetProcAddress(hModule, "LoadLibraryA");

    // 7、注入到指定进程中进行加载内存中申请的DLL信息，把LoadLibraryA的地址作为函数 来加载addr，也就是DLL的路径
    CreateRemoteThread(hProcess, NULL, 0, LPTHREAD_START_ROUTINE(FuncAddr), DllAddr, 0, NULL);
    return true;
}


BOOL CALLBACK MainDlgProc(HWND hWnd, UINT Message, WPARAM wPa, LPARAM lPa)  
{  
	BOOL bRet=FALSE;
	OPENFILENAME ofName;
	switch(Message)
	{
	case WM_CLOSE:
		EndDialog(hWnd,0);
		break;
	case WM_INITDIALOG:
		InitProcessListView(hWnd);
		InitModuleListView(hWnd);
		break;
	case WM_NOTIFY:
	{
		NMHDR* pNMHDR=(NMHDR*)lPa;
		if(wPa==IDC_LIST_PROCESS&&pNMHDR->code==NM_CLICK)
		{
			EnumModules(GetDlgItem(hWnd,IDC_LIST_PROCESS),GetDlgItem(hWnd,IDC_LIST_MOUDLE),wPa,lPa);
		}
		break;
	}
	case WM_COMMAND:
		switch(LOWORD(wPa))
		{
		case IDC_BUTTON_PE:
			{
				//打开PE文件

				
				LPCTSTR  lpstrFilter="*.EXE;*.DLL;*.SYS;.scr;.drv";
				char szBuffer[1024] = { 0 };

				memset(&ofName,0,sizeof(OPENFILENAME));
				ofName.lStructSize=sizeof(OPENFILENAME);
				ofName.lpstrFilter=lpstrFilter;
				ofName.lpstrFile = szBuffer;//存放文件的缓冲区
				ofName.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
				ofName.hwndOwner=hWnd;
				ofName.nMaxFile=MAX_PATH;

				GetOpenFileName(&ofName);

				filePath=(LPSTR)szBuffer;
				//打开新的对话框
				DialogBox(m_hInstance,MAKEINTRESOURCE(IDD_DIALOG_PEEDIT),NULL,PeEditDlgProc);

				break;
			}
		case IDC_BUTTON_ABOUT://关于
			break;
		case IDC_BUTTON_LOGOUT://退出
			EndDialog(hWnd,0);
			break;
		case IDC_BUTTON_ADDSHELL://程序加壳
			{
				LPCTSTR  lpstrFilter="*.exe;*.EXE;*.DLL;*.SYS;.scr;.drv";
				char szBuffer[1024] = { 0 };

				memset(&ofName,0,sizeof(OPENFILENAME));
				ofName.lStructSize=sizeof(OPENFILENAME);
				ofName.lpstrFilter=lpstrFilter;
				ofName.lpstrFile = szBuffer;//存放文件的缓冲区
				ofName.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
				ofName.hwndOwner=hWnd;
				ofName.nMaxFile=MAX_PATH;

				GetOpenFileName(&ofName);
				//scr文件路径
				filePath=(LPSTR)szBuffer;
				
				//获取shell路径
				//LPSTR shellPath="C:\\myfile\\project\\ShellCore\\Debug\\ShellCore.exe";

				
				TCHAR szFilePath[MAX_PATH + 1]={0};
				GetModuleFileName(NULL, szFilePath, MAX_PATH);
				(_tcsrchr(szFilePath, _T('\\')))[1] = 0; // 删除文件名，只获得路径字串
				
				LPSTR shellPath=lstrcat(szFilePath,"ShellCore.exe");
				

				LPVOID pFileBuffer = NULL;
				if (size_t fsize=ReadPEFile(filePath, &pFileBuffer))
				{
					//scr加密

					xor_crypt("zhangruyi",9,(char*)pFileBuffer,fsize);

					LPVOID pShellBuffer = NULL;
					if (size_t fsizeshell=ReadPEFile(shellPath, &pShellBuffer))
					{
						LPVOID pNewBuffer=NULL;
						//shell新增节
						if(size_t new_size=AddNewSection(pShellBuffer,&pNewBuffer,fsize,fsizeshell))
						{
							//将加密后的src程序追加到Shell程序的新增节中
							memcpy(PVOID(((DWORD)pNewBuffer)+fsizeshell),pFileBuffer,fsize);

							//LPSTR toPath = LPSTR("C:\\shellcode.exe");
							//保存加壳后的文件
							MemeryTOFile(pNewBuffer, new_size, filePath);

							free(pNewBuffer);
							pNewBuffer=NULL;
						}
						if(pNewBuffer!=NULL)
						{
							free(pNewBuffer);
							pNewBuffer=NULL;
						}
						free(pShellBuffer);
						pShellBuffer=NULL;
					}
					if(pShellBuffer!=NULL)
					{
						free(pShellBuffer);
						pShellBuffer=NULL;
					}
					free(pFileBuffer);
					pFileBuffer = NULL;
				}
				if(pFileBuffer!=NULL)
				{
					free(pFileBuffer);
					pFileBuffer = NULL;
				}
				break;
			}
		case IDC_BUTTON_DLLINJECT://DLL 注入
			{
				LPCTSTR  lpstrFilter="*.exe;*.EXE;*.DLL;*.SYS;.scr;.drv";
				char szBuffer[1024] = { 0 };

				memset(&ofName,0,sizeof(OPENFILENAME));
				ofName.lStructSize=sizeof(OPENFILENAME);
				ofName.lpstrFilter=lpstrFilter;
				ofName.lpstrFile = szBuffer;//存放文件的缓冲区
				ofName.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
				ofName.hwndOwner=hWnd;
				ofName.nMaxFile=MAX_PATH;

				GetOpenFileName(&ofName);

				unsigned long processId=GetSelectProcessId(GetDlgItem(hWnd,IDC_LIST_PROCESS));
				LoadDll(processId, (LPSTR)szBuffer);

				break;
			}
		}
	}

	return bRet;
} 

int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{
	m_hInstance=hInstance;

	INITCOMMONCONTROLSEX icex;
	icex.dwSize=sizeof(INITCOMMONCONTROLSEX);
	icex.dwICC=ICC_WIN95_CLASSES ;//ICC_WIN95_CLASSES;
	InitCommonControlsEx(&icex);//扩展控件

 	// TODO: Place code here.
	DialogBox(hInstance,MAKEINTRESOURCE(IDD_DIALOG_MAIN),NULL,MainDlgProc);
	return 0;
}



