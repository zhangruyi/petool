#include "pestudy.h"
#include<iostream>
#include "resource.h"


DWORD ReadPEFile(IN LPSTR lpszFile, OUT LPVOID* pFileBuffer)
{
	FILE* file = fopen(lpszFile, "r+b");
	if (file == NULL)
		return 0;
	int flength;
	fseek(file, 0, SEEK_END);
	flength = ftell(file);
	fseek(file, 0, SEEK_SET);
	*pFileBuffer = malloc(flength);
	if (pFileBuffer == NULL)
		return 0;
	size_t readSize= fread(*pFileBuffer, 1, flength, file);
	fclose(file);
	return flength;
}




DWORD CopyFileBufferToImageBuffer(IN LPVOID pFileBuffer, OUT LPVOID* pImageBuffer)
{
	//dos头
	_IMAGE_DOS_HEADER* pDosHeader;//dos头
	_IMAGE_NT_HEADERS* pNtHeader;//nt头

	DWORD dSectionPosition;

	//dos头
	pDosHeader = (_IMAGE_DOS_HEADER*)pFileBuffer;
	if (pDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
	{
		printf("读取的文件不是exe文件");
		return 0;
	}

	//NT头
	pNtHeader = (_IMAGE_NT_HEADERS*)((DWORD)pFileBuffer + pDosHeader->e_lfanew);
	if (pNtHeader->Signature != IMAGE_NT_SIGNATURE)
	{
		printf("读取的文件不是exe文件");
		return 0;
	}

	//节表位置
	dSectionPosition = (DWORD)pFileBuffer + pDosHeader->e_lfanew + sizeof(IMAGE_FILE_HEADER) + sizeof(DWORD) + pNtHeader->FileHeader.SizeOfOptionalHeader;
	
	
	//节表
	PIMAGE_SECTION_HEADER pSectionHeader = (PIMAGE_SECTION_HEADER)dSectionPosition;

	//分配内存
	//SizeOfImage:当镜像被加载进内存时的大小，包括所有的文件头。
	//向上舍入为SectionAlignment的倍数; 一般文件大小与加载到内存中的大小是不同的。
	*pImageBuffer = malloc(pNtHeader->OptionalHeader.SizeOfImage);
	if (*pImageBuffer == NULL)
		return 0;

	//
	memset(*pImageBuffer, 0, pNtHeader->OptionalHeader.SizeOfImage);
	//写入header 
	memcpy(*pImageBuffer, pFileBuffer, pNtHeader->OptionalHeader.SizeOfHeaders);

	
	//遍历节表
	while (pNtHeader->FileHeader.NumberOfSections--)
	{
		memcpy(PVOID((DWORD)*pImageBuffer + (pSectionHeader)->VirtualAddress) , PVOID((DWORD)pFileBuffer+(pSectionHeader)->PointerToRawData), pSectionHeader->SizeOfRawData);
		pSectionHeader++;
	}	
	return pNtHeader->OptionalHeader.SizeOfImage;
}



DWORD CopyImageBufferToNewBuffer(IN LPVOID pImageBuffer, OUT LPVOID* pNewBuffer)
{
	_IMAGE_DOS_HEADER* dosHeader;
	_IMAGE_NT_HEADERS* ntHeader;
	DWORD setionPosition;
	dosHeader = (_IMAGE_DOS_HEADER*)pImageBuffer;
	if (dosHeader->e_magic != IMAGE_DOS_SIGNATURE)
	{
		printf("内存缓冲区的文件格式有问题");
		return 0;
	}

	ntHeader = (_IMAGE_NT_HEADERS*)((DWORD)pImageBuffer+dosHeader->e_lfanew);
	if (ntHeader->Signature != IMAGE_NT_SIGNATURE)
	{
		printf("内存缓冲区中的文件格式有问题");
		return 0;
	}

	setionPosition = (DWORD)pImageBuffer + dosHeader->e_lfanew + 4 + sizeof(_IMAGE_FILE_HEADER) + ntHeader->FileHeader.SizeOfOptionalHeader;

	//节表
	PIMAGE_SECTION_HEADER pSectionHeader = (PIMAGE_SECTION_HEADER)setionPosition;
	//找到最后一个节表
	PIMAGE_SECTION_HEADER pLastSetion =(PIMAGE_SECTION_HEADER)(pSectionHeader + ntHeader->FileHeader.NumberOfSections - 1);

	size_t fileSize = pLastSetion->PointerToRawData + pLastSetion->SizeOfRawData;

	*pNewBuffer = malloc(fileSize);
	if (*pNewBuffer == NULL)
		return 0;

	memset(*pNewBuffer, 0, fileSize);

	//复制头信息
	memcpy(*pNewBuffer, pImageBuffer, ntHeader->OptionalHeader.SizeOfHeaders);

	//遍历节表
	while (ntHeader->FileHeader.NumberOfSections--)
	{
		memcpy(PVOID((DWORD)*pNewBuffer + (pSectionHeader)->PointerToRawData), PVOID((DWORD)pImageBuffer + (pSectionHeader)->VirtualAddress), pSectionHeader->SizeOfRawData);
		pSectionHeader++;
	}

	return fileSize;
}

BOOL MemeryTOFile(IN LPVOID pMemBuffer, IN size_t size, OUT LPSTR lpszFile)
{
    FILE* file= fopen(lpszFile, "wb");
	if (file == NULL)
	{
		printf("打开文件失败");
		return 0;
	}
	fwrite(pMemBuffer, size, 1, file);
	fclose(file);
	return 1;
}

//偏移位置转换
DWORD RvaToFileOffset(IN LPVOID pFileBuffer, IN DWORD dwRva)
{
	_IMAGE_DOS_HEADER* dosHeader;
	_IMAGE_NT_HEADERS* ntHeader;
	dosHeader = (_IMAGE_DOS_HEADER*)pFileBuffer;
	if (dosHeader->e_magic != IMAGE_DOS_SIGNATURE)
	{
		printf("您输入的内存数据不是PE格式数据");
		return 0;
	}

	ntHeader = (_IMAGE_NT_HEADERS*)((DWORD)pFileBuffer + dosHeader->e_lfanew);
	if (ntHeader->Signature != IMAGE_NT_SIGNATURE)
	{
		printf("您输入的内存数据不是PE格式数据");
		return 0;
	}

	if (ntHeader->OptionalHeader.FileAlignment == ntHeader->OptionalHeader.SectionAlignment)
		return dwRva+(DWORD)pFileBuffer;
	if (dwRva < ntHeader->OptionalHeader.SizeOfHeaders)
		return dwRva + (DWORD)pFileBuffer;

	DWORD setionPosition;
	setionPosition = (DWORD)pFileBuffer + dosHeader->e_lfanew + 4 + sizeof(_IMAGE_FILE_HEADER) + ntHeader->FileHeader.SizeOfOptionalHeader;
	//节表
	PIMAGE_SECTION_HEADER pSectionHeader = (PIMAGE_SECTION_HEADER)setionPosition;
	while (--ntHeader->FileHeader.NumberOfSections)
	{
		PIMAGE_SECTION_HEADER sectionRow = pSectionHeader + ntHeader->FileHeader.NumberOfSections;
		if (dwRva > sectionRow->VirtualAddress)
			return dwRva - sectionRow->VirtualAddress + sectionRow->PointerToRawData + (DWORD)pFileBuffer;
	}
	return dwRva + (DWORD)pFileBuffer;
}


BOOL ReadPeStruc(HWND hWnd,LPSTR lpszFile)
{

    LPVOID pFileBuffer;
	if(ReadPEFile(lpszFile,&pFileBuffer))
	{
		//dos头
		_IMAGE_DOS_HEADER* pDosHeader;//dos头
		_IMAGE_NT_HEADERS* pNtHeader;//nt头

		DWORD dSectionPosition;

		//dos头
		pDosHeader = (_IMAGE_DOS_HEADER*)pFileBuffer;
		if (pDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
		{
			printf("读取的文件不是exe文件");
			return 0;
		}

		//NT头
		pNtHeader = (_IMAGE_NT_HEADERS*)((DWORD)pFileBuffer + pDosHeader->e_lfanew);
		if (pNtHeader->Signature != IMAGE_NT_SIGNATURE)
		{
			printf("读取的文件不是exe文件");
			return 0;
		}

		//节表位置
		dSectionPosition = (DWORD)pFileBuffer + pDosHeader->e_lfanew + sizeof(IMAGE_FILE_HEADER) + sizeof(DWORD) + pNtHeader->FileHeader.SizeOfOptionalHeader;
		
		
		//节表
		PIMAGE_SECTION_HEADER pSectionHeader = (PIMAGE_SECTION_HEADER)dSectionPosition;

		TCHAR sText[10] = {0};

		//入口点
		HWND hEntrance;
		hEntrance=GetDlgItem(hWnd,IDC_EDIT_ENTRANCE);
		sprintf(sText, "%08X", pNtHeader->OptionalHeader.AddressOfEntryPoint);
		::SetWindowTextA(hEntrance,sText);

		//基地址
		HWND hImageBase=GetDlgItem(hWnd,IDC_EDIT_IMAGEBASE);
		memset(sText,0,10);
		sprintf(sText, "%08X", pNtHeader->OptionalHeader.ImageBase);
		::SetWindowTextA(hImageBase,sText);

		//镜像大小
		HWND hSizeOfImage=GetDlgItem(hWnd,IDC_EDIT_SIZEOFIMAGE);
		memset(sText,0,10);
		sprintf(sText, "%08X", pNtHeader->OptionalHeader.SizeOfImage);
		::SetWindowTextA(hSizeOfImage,sText);

		//代码基址 IDC_EDIT_BASEOFCODE
		HWND hBaseOfCode=GetDlgItem(hWnd,IDC_EDIT_BASEOFCODE);
		memset(sText,0,10);
		sprintf(sText, "%08X", pNtHeader->OptionalHeader.BaseOfCode);
		::SetWindowTextA(hBaseOfCode,sText);

		//数据基址 IDC_EDIT_BASEOFDATA
		HWND hBaseOfData=GetDlgItem(hWnd,IDC_EDIT_BASEOFDATA);
		memset(sText,0,10);
		sprintf(sText, "%08X", pNtHeader->OptionalHeader.BaseOfData);
		::SetWindowTextA(hBaseOfData,sText);

		//内存块对其 IDC_EDIT_SECTIONALIGNMENT
		HWND hSectionAlignment=GetDlgItem(hWnd,IDC_EDIT_SECTIONALIGNMENT);
		memset(sText,0,10);
		sprintf(sText, "%08X", pNtHeader->OptionalHeader.SectionAlignment);
		::SetWindowTextA(hSectionAlignment,sText);

		//文件对其 IDC_EDIT_FILEALIGNMENT
		HWND hFileAlignment=GetDlgItem(hWnd,IDC_EDIT_FILEALIGNMENT);
		memset(sText,0,10);
		sprintf(sText,"%08X",pNtHeader->OptionalHeader.FileAlignment);
		::SetWindowText(hFileAlignment,sText);

		//标志字 IDC_EDIT_MAGIC
		HWND hMagic=GetDlgItem(hWnd,IDC_EDIT_MAGIC);
		memset(sText,0,10);
		sprintf(sText,"%04X",pNtHeader->OptionalHeader.Magic);
		::SetWindowText(hMagic,sText);



		//子系统 IDC_EDIT_SUBSYSTEM
		HWND hSubSystem=GetDlgItem(hWnd,IDC_EDIT_SUBSYSTEM);
		memset(sText,0,10);
		sprintf(sText,"%04X",pNtHeader->OptionalHeader.Subsystem);
		::SetWindowText(hSubSystem,sText);

		//区段数目 IDC_EDIT_NUMBEROFSECTION
		HWND hNumberOfSection=GetDlgItem(hWnd,IDC_EDIT_NUMBEROFSECTION);
		memset(sText,0,10);
		sprintf(sText,"%04X",pNtHeader->FileHeader.NumberOfSections);
		::SetWindowText(hNumberOfSection,sText);

		//日期时间标 IDC_EDIT_TIMEDATESTAMP
		HWND hTimeDateStamp=GetDlgItem(hWnd,IDC_EDIT_TIMEDATESTAMP);
		memset(sText,0,10);
		sprintf(sText,"%08X",pNtHeader->FileHeader.TimeDateStamp);
		::SetWindowText(hTimeDateStamp,sText);

		//首部大小 头大小 IDC_EDIT_SIZEOFHEADER
		HWND hSizeOfHeader=GetDlgItem(hWnd,IDC_EDIT_SIZEOFHEADER);
		memset(sText,0,10);
		sprintf(sText,"%08X",pNtHeader->OptionalHeader.SizeOfHeaders);
		::SetWindowText(hSizeOfHeader,sText);

		//特征值 IDC_EDIT_FILLCHARACTERISTICS
		HWND hCharacteristics=GetDlgItem(hWnd,IDC_EDIT_FILLCHARACTERISTICS);
		memset(sText,0,10);
		sprintf(sText,"%04X",pNtHeader->FileHeader.Characteristics);
		::SetWindowText(hCharacteristics,sText);

		//校验和 IDC_EDIT_CHECKSUM
		HWND hCheckSum=GetDlgItem(hWnd,IDC_EDIT_CHECKSUM);
		memset(sText,0,10);
		sprintf(sText,"%08X",pNtHeader->OptionalHeader.CheckSum);
		::SetWindowText(hCheckSum,sText);

		//可选头大小 IDC_EDIT_SIZEOFOPTIONHEADER
		HWND hSizeOfOptionHeader=GetDlgItem(hWnd,IDC_EDIT_SIZEOFOPTIONHEADER);
		memset(sText,0,10);
		sprintf(sText,"%04X",pNtHeader->FileHeader.SizeOfOptionalHeader);
		::SetWindowText(hSizeOfOptionHeader,sText);

		//RVA 数及大小 IDC_EDIT_NUMBEROFRVAANDSIZE
		HWND hNumberOfRvaAndSize=GetDlgItem(hWnd,IDC_EDIT_NUMBEROFRVAANDSIZE);
		memset(sText,0,10);
		sprintf(sText,"%08X",pNtHeader->OptionalHeader.NumberOfRvaAndSizes);
		::SetWindowText(hNumberOfRvaAndSize,sText);


		/*
		//遍历节表
		while (pNtHeader->FileHeader.NumberOfSections--)
		{
			pSectionHeader++;
		}
		*/
	}
	return 1;
}


DWORD AddNewSection(IN LPVOID pFileBuffer, OUT LPVOID* pImageBuffer,size_t size,size_t shellsize)
{
	PIMAGE_DOS_HEADER pImageDosHeader = (PIMAGE_DOS_HEADER)pFileBuffer;
	if (pImageDosHeader->e_magic != IMAGE_DOS_SIGNATURE)
	{
		printf("内存缓冲区MZ格式有问题");
		return 0;
	}

	PIMAGE_NT_HEADERS pImageNtHeader;
	PIMAGE_SECTION_HEADER pImageSectionHeader;
	pImageNtHeader = (PIMAGE_NT_HEADERS)(DWORD(pFileBuffer) + pImageDosHeader->e_lfanew);
	if (pImageNtHeader->Signature != IMAGE_NT_SIGNATURE)
	{
		printf("内存缓冲区NT格式有问题");
		return 0;
	}

	pImageSectionHeader = IMAGE_FIRST_SECTION(pImageNtHeader);
	
	PIMAGE_SECTION_HEADER pLastSectionHeader = pImageSectionHeader + pImageNtHeader->FileHeader.NumberOfSections - 1;

	//判断节表后是否有空余位置
	bool isHavSpace=true;
	for (size_t i = 0; i < 20; i++)
	{
		long* pDwordItem=(long*)(pLastSectionHeader + 1) + i;
		if (*pDwordItem > 0)
		{
			isHavSpace = false;
			break;
		}
	}

	if (!isHavSpace)
	{
		//判断dos头后有没有空闲的地方
		if (pImageDosHeader->e_lfanew - sizeof(pImageDosHeader) > IMAGE_SIZEOF_SECTION_HEADER * 2)
		{
			DWORD dwordStart1 = DWORD(pFileBuffer) + sizeof(pImageDosHeader);
			DWORD dwordStart2 = DWORD(pFileBuffer) + pImageDosHeader->e_lfanew;
			size_t movesize = (DWORD)(pLastSectionHeader + 1) - dwordStart2;
			memcpy((PVOID)dwordStart1, (PVOID)dwordStart2, movesize);
			pImageDosHeader->e_lfanew = sizeof(pImageDosHeader);

			pImageNtHeader = (PIMAGE_NT_HEADERS)(DWORD(pFileBuffer) + pImageDosHeader->e_lfanew);
			if (pImageNtHeader->Signature != IMAGE_NT_SIGNATURE)
			{
				printf("内存缓冲区NT格式有问题");
				return 0;
			}

			pImageSectionHeader = IMAGE_FIRST_SECTION(pImageNtHeader);

			pLastSectionHeader = pImageSectionHeader + pImageNtHeader->FileHeader.NumberOfSections - 1;
			isHavSpace = true;
		}
	}
	
	if (isHavSpace)
	{

		size_t sfag=(size_t)pImageNtHeader->OptionalHeader.FileAlignment;
		//计算新增节数量
		size_t newcount = 
			((int)(size/ sfag))+
			((size% sfag)>0?1:0);

		//分配新的空间
		size_t newsize = shellsize + (
			newcount*pImageNtHeader->OptionalHeader.FileAlignment);

		//修改内存缓冲区，新增一个节表
		PIMAGE_SECTION_HEADER pNewSectionHeader = pLastSectionHeader + 1;
		pNewSectionHeader->Characteristics = pLastSectionHeader->Characteristics;
		
		memcpy(pNewSectionHeader, ".zry", 4);
		pNewSectionHeader->PointerToRawData = shellsize;
		pNewSectionHeader->SizeOfRawData = newcount * pImageNtHeader->OptionalHeader.FileAlignment;
		pNewSectionHeader->VirtualAddress = pImageNtHeader->OptionalHeader.SizeOfImage;
		pNewSectionHeader->Misc.VirtualSize =size;//pNewSectionHeader->SizeOfRawData;//newcount * pImageNtHeader->OptionalHeader.SectionAlignment;

		pImageNtHeader->FileHeader.NumberOfSections = pImageNtHeader->FileHeader.NumberOfSections + 1;

		pImageNtHeader->OptionalHeader.SizeOfImage+=	newcount * pImageNtHeader->OptionalHeader.SectionAlignment;
	

		*pImageBuffer = malloc(newsize);
		if (*pImageBuffer == NULL)
			return 0;
		memset(*pImageBuffer, 0, newsize);
		memcpy(*pImageBuffer, pFileBuffer, shellsize);
		return newsize;
	}
	return 0;
}

