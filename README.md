# PETool

#### 介绍
Pe查看工具、进程和进程中模块查看基地址和镜像大小、Dll注入

#### 软件架构
VC++、WindowsXP


#### 安装教程

直接打开exe，不需要安装

#### 使用说明

1.  打开后上半部分显示进程列表，下半部分的列表是当前选中进程的模块列表
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/141113_3912459b_1758276.png "屏幕截图.png")
2.  PE查看，点击PE查看按钮弹出一个文件选择对话框，选择一个PE文件，点去确定，弹出此文件的PE信息
![输入图片说明](https://images.gitee.com/uploads/images/2021/0520/141318_6157fb21_1758276.png "屏幕截图.png")
3.  程序加壳，把要加壳的程序字节流加密后放到一个提前准备好的ShellCore.exe的新增节中，ShellCore.exe实现将新节点中的字节解密后把Imagebase位置指向新地址
4.  DLL注入：选中一个进程，点击dll注入，弹出文件选择对话框，选中要注入的dll

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 详细说明请参考文章

1.  PE结构拉伸内存：https://blog.csdn.net/cszrydn/article/details/117075316
2.  DLL注入：https://blog.csdn.net/cszrydn/article/details/117075561

